#!/usr/bin/env bash
# TODO: Rewrite in python to make this more platform independent.

################# Build image #################

# Check if the docker image exists and build it if it doesn't.
if [[ $@ == *--no-cache* ]];
then
    . ./build_docker.sh --no-cache || exit 2
else
    . ./build_docker.sh || exit 2
fi


if [ -z "$IMAGE_NAME" ];
then
    IMAGE_NAME=$PROJECT_CATEGORY/$PROJECT_NAME:$PROJECT_VERSION
fi

################# Create container #################

DOCKER_XSOCK=/tmp/.X11-unix
DOCKER_XAUTH=/tmp/.docker.xauth
xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f $DOCKER_XAUTH nmerge -

if [ -z "$DOCKER_DISPLAY" ];
then
    if [ -z "$DISPLAY" ];
    then
        DISPLAY="127.0.0.1:0.0"
    fi
    DOCKER_DISPLAY=$DISPLAY
fi
if [ -z "$DOCKER_WEBCAM" ];
then
    DOCKER_WEBCAM=/dev/video0
fi
if [ -z "$DOCKER_SOUND" ];
then
    DOCKER_SOUND="--device /dev/snd \
	-e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native \
	-v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native \
	--group-add $(getent group audio | cut -d: -f3)"

fi

# Create a container instance.
# Enable GUI via X11 forwarding.
# Also, mount this folder so that we can run our program.
echo "Creating $PROJECT_NAME container instance from image $PROJECT_CATEGORY/$PROJECT_NAME:$PROJECT_VERSION."
nvidia-docker create \
    -v $SCRIPTPATH:/root/$PROJECT_NAME \
    -v $DOCKER_XSOCK:$DOCKER_XSOCK \
    -v $DOCKER_XAUTH:$DOCKER_XAUTH \
    -e XAUTHORITY=$DOCKER_XAUTH \
    -e DISPLAY=$DOCKER_DISPLAY \
    -e NO_AT_BRIDGE=1 \
    -e QT_X11_NO_MITSHM=1 \
    -P \
    --ipc host \
    $DOCKER_SOUND \
    --privileged \
    --device $DOCKER_WEBCAM \
    --name $PROJECT_NAME \
    -it $IMAGE_NAME

if [ -z "$1" ] || [[ $@ == -* ]];
then
    # Set default command.
    CMD="bash"
else
    # Use user command.
    CMD="$@"
fi

# Now start it.
docker start $PROJECT_NAME

# Now attach bash session.
docker exec -it $PROJECT_NAME /entrypoint.sh $CMD

# Clean up this image.
docker rm -f $PROJECT_NAME
