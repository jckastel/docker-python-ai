#!/usr/bin/env bash

STEP_NAME="tools-ai"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
MD5_FILES="$STEP_PATH/packages.txt"
RETRIES=5

################# Build image #################

. $SCRIPTPATH/build-step.sh
