#!/usr/bin/env bash

STEP_NAME="tensorflow"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

TENSORFLOW_VERSION="$(cat $STEP_PATH/tensorflow.version.txt)"

MD5_FILES="$STEP_PATH/tensorflow.version.txt $STEP_PATH/packages.txt $STEP_PATH/requirements.txt $STEP_PATH/docker_requirements/*"
BUILD_ARGS="TENSORFLOW_VERSION=$TENSORFLOW_VERSION"
RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
