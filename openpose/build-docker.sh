#!/usr/bin/env bash

STEP_NAME="openpose"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
OPENPOSE_VERSION="$(cat $STEP_PATH/openpose.version.txt | tr -d '[:space:]')"
RMPPE_VERSION="$(cat $STEP_PATH/rmppe.version.txt | tr -d '[:space:]')"
TF_OPENPOSE_VERSION="$(cat $STEP_PATH/tf_openpose.version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/*.txt $STEP_NAME/docker_requirements/openpose/config.m"
BUILD_ARGS="OPENPOSE_VERSION=$OPENPOSE_VERSION RMPPE_VERSION=$RMPPE_VERSION TF_OPENPOSE_VERSION=$TF_OPENPOSE_VERSION"
RETRIES=5

################# Build image #################

. $SCRIPTPATH/build-step.sh
