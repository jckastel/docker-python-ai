#!/usr/bin/env bash
# TODO: Rewrite in python to make this more platform independent.
PROJECT_NAME=python-ai
PROJECT_CATEGORY=datascience
# Get the path to this script.
SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Read version info from file.
PROJECT_VERSION="$(cat $SCRIPTPATH/version.txt | tr -d '[:space:]')"


echo -e "\033[0;35m$PROJECT_NAME path is $SCRIPTPATH\033[0m"

################# Check dependencies #################

. $SCRIPTPATH/check_requirements.sh || exit 1

################# Build steps #################

unset -v PREVIOUS_STEP_NAME PREVIOUS_IMAGE_NAME

for STEP_NAME in base `cat $SCRIPTPATH/steps.txt | egrep -v "^\s*(#|$)"` $PROJECT_NAME; do
    unset -v MD5_FILES BUILD_ARGS RETRY_SECONDS RETRIES
    . $SCRIPTPATH/$STEP_NAME/build-docker.sh || exit 2;
done

docker system prune -f
