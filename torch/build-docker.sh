#!/usr/bin/env bash

STEP_NAME="torch"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
TORCH_VERSION="$(cat $STEP_PATH/torch.version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/*.txt"
BUILD_ARGS="TORCH_VERSION=$TORCH_VERSION"

RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
