#!/usr/bin/env bash

STEP_NAME="pytorch"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

PYTORCH_VERSION="$(cat $STEP_PATH/pytorch.version.txt | tr -d '[:space:]')"
TORCHVISION_VERSION="$(cat $STEP_PATH/torchvision.version.txt | tr -d '[:space:]')"

# Set version info.
MD5_FILES="$STEP_PATH/*.txt"
BUILD_ARGS="PYTORCH_VERSION=$PYTORCH_VERSION TORCHVISION_VERSION=$TORCHVISION_VERSION"

RETRIES=5

################# Build image #################

. $SCRIPTPATH/build-step.sh
