#!/usr/bin/env bash

STEP_NAME="xgboost"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
XGBOOST_VERSION="$(cat $STEP_PATH/version.txt | tr -d '[:space:]')"
if [ "$(cat /proc/cpuinfo | grep avx)" == "" ];
then
    USE_AVX="OFF";
else
    USE_AVX="ON";
fi

MD5_FILES="$STEP_PATH/version.txt"
BUILD_ARGS="XGBOOST_VERSION=$XGBOOST_VERSION USE_AVX=$USE_AVX"

RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
