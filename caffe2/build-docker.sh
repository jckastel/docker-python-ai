#!/usr/bin/env bash

STEP_NAME="caffe2"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
PYTORCH_VERSION="$(cat $STEP_PATH/version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/version.txt $STEP_PATH/requirements.txt $STEP_PATH/packages.txt"
BUILD_ARGS="PYTORCH_VERSION=$PYTORCH_VERSION"

################# Build image #################

. $SCRIPTPATH/build-step.sh
