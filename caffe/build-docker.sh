#!/usr/bin/env bash

STEP_NAME="caffe"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
CAFFE_VERSION="$(cat $STEP_PATH/version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/version.txt $STEP_NAME/docker_requirements/caffe/Makefile.config"
BUILD_ARGS="CAFFE_VERSION=$CAFFE_VERSION"

################# Build image #################

. $SCRIPTPATH/build-step.sh
