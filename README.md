# docker-python-datascience

A general purpose docker data science container for use with python. 

Includes most of the common machine learning and AI libraries available in python 3.5 (and 2.7) with CUDA 9.0 and CuDNN 7 support (requires a compatible graphics card and drivers to be installed on the host machine).

Build it with build-docker.sh. Warning: This will take several hours and the resulting image will be over 20GB.

Run it with run-docker.sh [my_cmd] (starts bash by default if no command is supplied). 
If the image does not exist, or if any Dockerfile, *version.txt, packages.txt or requirement*.txt files change, it will be rebuilt automatically. 
Removing the auto-generated .build files will also cause the image for the corresponding build step (and any downstream build step image) to be rebuilt.

If you want to customize the build you can skip or change the order of some build steps (though this is untested and other steps might fail if you do) by editing steps.txt.

You can add, update, or remove dependencies by editing the various txt files in the subdirectories corresponding to each build step. This will cause the image to rebuild on the next run.