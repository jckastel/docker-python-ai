#!/usr/bin/env bash

STEP_NAME="deeppose"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
DEEPPOSE_VERSION="$(cat $STEP_PATH/deeppose.version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/*.txt"
BUILD_ARGS="DEEPPOSE_VERSION=$DEEPPOSE_VERSION"
RETRIES=5

################# Build image #################

. $SCRIPTPATH/build-step.sh
