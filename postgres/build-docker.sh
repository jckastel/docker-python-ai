#!/usr/bin/env bash

STEP_NAME="postgres"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
POSTGRES_VERSION="$(cat $STEP_PATH/postgres.version.txt | tr -d '[:space:]')"
POSTGRES_PORT="$(cat $STEP_PATH/postgres.port.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/*.txt"
BUILD_ARGS="POSTGRES_VERSION=$POSTGRES_VERSION POSTGRES_PORT=$POSTGRES_PORT"
RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
