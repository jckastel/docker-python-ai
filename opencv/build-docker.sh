#!/usr/bin/env bash

STEP_NAME="opencv"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
OPENCV_VERSION="$(cat $STEP_PATH/version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/version.txt"
BUILD_ARGS="OPENCV_VERSION=$OPENCV_VERSION"

################# Build image #################

. $SCRIPTPATH/build-step.sh
