#!/usr/bin/env bash
# TODO: Rewrite in python to make this more platform independent.

# Check if nvidia-docker is installed.
if [ $(dpkg-query -W -f='${Status}' nvidia-docker 2>/dev/null | grep -c "installed") -eq 0 ];
then
    # Tell user to install nvidia-docker.
    echo -e "\033[0;31mNvidia-docker not found; On ubuntu you can install it by using 'apt-get install nvidia-modprobe nvidia-docker'.\033[0m"
    exit 1
fi
echo -e "\033[0;33mNvidia-docker found.\033[0m"