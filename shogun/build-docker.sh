#!/usr/bin/env bash

STEP_NAME="shogun"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
SHOGUN_VERSION="$(cat $STEP_PATH/version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/version.txt"
BUILD_ARGS="SHOGUN_VERSION=$SHOGUN_VERSION"

RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
