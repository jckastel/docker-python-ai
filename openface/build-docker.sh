#!/usr/bin/env bash

STEP_NAME="openface"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
OPENFACE_VERSION="$(cat $STEP_PATH/openface.version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/*.txt"
BUILD_ARGS="OPENFACE_VERSION=$OPENFACE_VERSION"
RETRIES=5

################# Build image #################

. $SCRIPTPATH/build-step.sh
