#!/usr/bin/env bash

STEP_NAME="densepose"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
DENSEPOSE_VERSION="$(cat $STEP_PATH/version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/*.txt"
BUILD_ARGS="DENSEPOSE_VERSION=$DENSEPOSE_VERSION"
RETRIES=5

################# Build image #################

. $SCRIPTPATH/build-step.sh
