#!/usr/bin/env bash

STEP_NAME="realsense"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
LIBREALSENSE_VERSION="$(cat $STEP_PATH/version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/version.txt"
BUILD_ARGS="LIBREALSENSE_VERSION=$LIBREALSENSE_VERSION"

RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
