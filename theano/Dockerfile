ARG     LIBGPUARRAY_VERSION
ARG     PYDOT_NG_VERSION=master

# Add config file for theano.
COPY    theano/docker_requirements/.theanorc /root/.theanorc
COPY    theano/requirements.txt /root/theano_requirements.txt
COPY    theano/packages.txt /root/theano_packages.txt

RUN     echo "\033[0;32mBuilding Theano image ${VERSION}.\033[0m" \
        && if [ `pip2 freeze | grep theano==` ]; then pip2 uninstall -y theano; fi \
        && if [ `pip3 freeze | grep theano==` ]; then pip3 uninstall -y theano; fi \
        && apt-get update \
        && DEBIAN_FRONTEND=noninteractive apt-get install -y \
            $(cat /root/theano_packages.txt | egrep -v "^\s*(#|$)") \
        && apt-get clean

RUN     echo "\033[0;35mInstalling theano.\033[0m" \
        && pip2 install --upgrade -r /root/theano_requirements.txt \
        && pip3 install --upgrade -r /root/theano_requirements.txt \
        && rm /root/theano_requirements.txt

RUN     echo "\033[0;35mInstalling pydot-ng version ${PYDOT_NG_VERSION}.\033[0m" \
        && git clone https://github.com/pydot/pydot-ng.git /root/pydot-ng \
        && cd /root/pydot-ng \
        && git checkout $PYDOT_NG_VERSION \
        && python2 setup.py install \
        && python3 setup.py install

RUN     echo "\033[0;35mInstalling libgpuarray version ${LIBGPUARRAY_VERSION}.\033[0m" \
        && git clone https://github.com/Theano/libgpuarray.git /root/libgpuarray \
        && cd /root/libgpuarray \
        && git checkout $LIBGPUARRAY_VERSION \
        && mkdir Build \
        && cd Build \
        && cmake .. -DCMAKE_BUILD_TYPE=Release \
        && make -j $(($(nproc) + 1)) \
        && make install \
        && cd .. \
        && ldconfig \
        && python2 setup.py build \
        && python2 setup.py install \
        && python3 setup.py build \
        && python3 setup.py install \
        && rm /root/theano_packages.txt
