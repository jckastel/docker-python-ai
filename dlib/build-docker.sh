#!/usr/bin/env bash

STEP_NAME="dlib"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
DLIB_VERSION="$(cat $STEP_PATH/version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/version.txt"
BUILD_ARGS="DLIB_VERSION=$DLIB_VERSION"

################# Build image #################

. $SCRIPTPATH/build-step.sh
