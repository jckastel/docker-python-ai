#!/usr/bin/env bash

STEP_NAME="torch-libs"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
MD5_FILES="$STEP_PATH/*.txt"

RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
