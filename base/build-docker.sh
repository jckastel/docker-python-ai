#!/usr/bin/env bash
STEP_NAME="base"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

# Set version info.
CUDA_IMAGE_VERSION="$(cat $STEP_PATH/cuda.version.txt | tr -d '[:space:]')"

MD5_FILES="$STEP_PATH/*.txt $STEP_PATH/entrypoint.sh $STEP_PATH/docker_requirements/*"
BUILD_ARGS="CUDA_IMAGE_VERSION=$CUDA_IMAGE_VERSION"
RETRIES=5

################# Build image #################

. $SCRIPTPATH/build-step.sh
