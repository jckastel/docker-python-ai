#!/usr/bin/env bash
# TODO: Rewrite in python to make this more platform independent.
echo -e "\033[0;35mBuilding step $STEP_NAME\033[0m"
# Determine the name:tag of the current image.
IMAGE_NAME="$PROJECT_CATEGORY/$STEP_NAME:$PROJECT_VERSION"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"
DOCKER_FILE="$STEP_PATH/Dockerfile"

# Set default retry arguments.
if [ -z $RETRY_SECONDS ]; then
    RETRY_SECONDS=60
fi
if [ -z $RETRIES ]; then
    RETRIES=0
fi

if [ -z "$MD5_FILES" ]; then
    MD5_FILES=""
fi
# Add default files to MD5 Hash file name list.
MD5_FILES="$DOCKER_FILE $MD5_FILES"

if [ -z "$BUILD_ARGS" ]; then
    BUILD_ARGS=""
fi
if [ -z "$BUILD_FLAGS" ]; then
    BUILD_FLAGS=""
fi

# Add more default arguments and MD5 files based on previous step.
if [ ! -z $PREVIOUS_STEP_NAME ];
then
    echo -e "\033[1;35mPrevious step was $PREVIOUS_STEP_NAME\033[0m"
    # This image is built on a previous build step, so find the name:tag of that step.
    PARENT_IMAGE="$PROJECT_CATEGORY/$PREVIOUS_STEP_NAME"
    # Add a set of default build args.
    BUILD_ARGS="$BUILD_ARGS PARENT_IMAGE=$PARENT_IMAGE"
fi

# Find MD5 of the previous step.
PARENT_IMAGE_MD5=""
if [ ! -z $PREVIOUS_IMAGE_NAME ];
then
    PARENT_IMAGE_MD5=" $(docker inspect --format='{{.Config.Labels.md5}}' $PREVIOUS_IMAGE_NAME | tr -d '[:space:]')"
fi
# Construct MD5 checksum to check whether project docker images are up-to-date.
DOCKERFILE_MD5=`echo "$(md5sum $MD5_FILES | awk '{print $1}')$PARENT_IMAGE_MD5" | md5sum | awk '{print $1}'`
LAST_DOCKERFILE_MD5=""
if [ ! -z $(nvidia-docker images -q $IMAGE_NAME) ]; then
    LAST_DOCKERFILE_MD5="$(docker inspect --format='{{.Config.Labels.md5}}' $IMAGE_NAME | tr -d '[:space:]')"
fi

# Add a set of default build args.
BUILD_ARGS="DOCKERFILE_MD5=$DOCKERFILE_MD5 $BUILD_ARGS VERSION=$PROJECT_VERSION"

BUILD_ARGS_STRING=""
for ARG in $BUILD_ARGS; do
    if [ ! -z "$BUILD_ARGS_STRING" ];
    then
        BUILD_ARGS_STRING+=" "
    fi
    BUILD_ARGS_STRING+="--build-arg $ARG"
done

################# Build docker image step #################

# Report some settings for this build step.
echo -e "\033[1;35m$STEP_NAME path: $STEP_PATH\033[0m"
echo -e "\033[1;35mImage name: $IMAGE_NAME\033[0m"
echo -e "\033[1;35mBuild arguments: $BUILD_ARGS_STRING\033[0m"
echo -e "\033[1;35mDocker file: $DOCKER_FILE\033[0m"
echo -e "\033[1;35mMD5: $DOCKERFILE_MD5\033[0m"
echo -e "\033[1;35mLast MD5: $LAST_DOCKERFILE_MD5\033[0m"
echo -e "\033[1;35mFiles for MD5: $MD5_FILES\033[0m"
echo -e "\033[1;35mRetries: $RETRIES ($RETRY_SECONDS second wait time)\033[0m"

# Check if the docker image exists.
if [ -z $(nvidia-docker images -q $IMAGE_NAME) ] || [ "$DOCKERFILE_MD5" != "$LAST_DOCKERFILE_MD5" ];
then
    echo -e "\033[1;34mLatest $STEP_NAME docker image $IMAGE_NAME with hash $DOCKERFILE_MD5 for version $PROJECT_VERSION does not exist, building it\033[0m"
    # Build the docker image.
    COUNTER=0

    # Retry until it is built or until we run out of retries, waiting a little while between builds.
    RENDERED_DOCKERFILE="$STEP_PATH/Dockerfile.tmp"
    if [ -f $RENDERED_DOCKERFILE ]; then rm $RENDERED_DOCKERFILE; fi

    RENDERED=`cat $DOCKER_FILE`
    if [ $STEP_NAME != 'base' ]; then
        DOCKER_TEMPLATE=`cat Dockerfile.template`
        RENDERED="${DOCKER_TEMPLATE}"$'\n'"${RENDERED}"$'\n'"RUN     rm -rf /tmp/*; exit 0"$'\n'"RUN     rm -rf /root/.cache; exit 0"$'\n'"RUN     rm -rf /root/.ccache; exit 0"
    fi
    echo "$RENDERED"
    rm $RENDERED_DOCKERFILE
    echo "$RENDERED" > $RENDERED_DOCKERFILE
    until nvidia-docker build $BUILD_ARGS_STRING $BUILD_FLAGS -t $IMAGE_NAME -f $RENDERED_DOCKERFILE $SCRIPTPATH $@ || { (( COUNTER++ >= $RETRIES )) && { exit 1; } }; do echo -e "\033[0;33mTry $STEP_NAME again in $RETRY_SECONDS seconds ($COUNTER/$RETRIES)\033[0m"; sleep $RETRY_SECONDS; done || exit 2

    if [ -f $RENDERED_DOCKERFILE ]; then rm $RENDERED_DOCKERFILE; fi
    unset -v COUNTER
else
    echo -e "\033[1;32mLatest $STEP_NAME docker image $IMAGE_NAME with hash $DOCKERFILE_MD5 for version $PROJECT_VERSION exists, moving to next step.\033[0m"
fi

# Remove any dangling images.
DANGLING=$(docker images -a --filter=dangling=true -q)
if [ ! -z "$DANGLING" ];
then
    docker rmi -f $DANGLING
fi

# Unset some variables that we no longer need.
unset -v RETRIES RETRY_SECONDS DANGLING
# Update the previous step name.
PREVIOUS_STEP_NAME=$STEP_NAME
PREVIOUS_IMAGE_NAME=$IMAGE_NAME
