#!/usr/bin/env bash

STEP_NAME="update-python"

MD5_FILES="$SCRIPTPATH/tools-ai-py2/requirements.txt $SCRIPTPATH/tools-ai-py3/requirements.txt $SCRIPTPATH/requirements-py.txt"
RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
