#!/usr/bin/env bash
STEP_NAME="nltk"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

NLTK_VERSION="$(cat $STEP_PATH/version.txt)"

MD5_FILES="$STEP_PATH/*.txt"
BUILD_ARGS="NLTK_VERSION=$NLTK_VERSION"
# NLTK data downloads fails sometimes without producing an error code.
# If the cache is used, the data will be unavailable.
BUILD_FLAGS="--no-cache"
RETRIES=5

################# Build image #################

. $SCRIPTPATH/build-step.sh
