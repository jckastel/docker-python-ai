#!/usr/bin/env bash

STEP_NAME="theano"
STEP_PATH="$SCRIPTPATH/$STEP_NAME"

LIBGPUARRAY_VERSION="$(cat $STEP_PATH/libgpuarray.version.txt)"
PYDOT_NG_VERSION="$(cat $STEP_PATH/pydot-ng.version.txt)"

MD5_FILES="$STEP_PATH/*.txt $STEP_NAME/docker_requirements/.theanorc"
BUILD_ARGS="LIBGPUARRAY_VERSION=$LIBGPUARRAY_VERSION PYDOT_NG_VERSION=$PYDOT_NG_VERSION"
RETRIES=2

################# Build image #################

. $SCRIPTPATH/build-step.sh
